console.clear();


function c(msg) { 
  console.log(msg) 
  //console.log(arguments[1])
}


// generators
function* genny() {
  yield 'a'
  yield 'b'
  yield 'c'
}

function* counterf({ num }) {
  while (num > 0) {
    yield num
    num--
  }
}

for(let f of counterf({num:10})) {
  c(f)
}

let iter = genny()
for(let f of iter) {
  c(f)
}
iter = genny()
let counter = 20
let next = iter.next()
while (next.done != true && counter > 0) {
  c(next.value)
  next = iter.next()
  counter --;
}

class Numbers {
  
	constructor({min, max}) {
    this.min = min
    this.max = max
  }
 *[Symbol.iterator]() {
        for (let i = this.min; i <= this.max; i++) {
           yield i
        }
    }
}

const nums = new Numbers({min:4, max:12})
for(let f of nums) {
  c(f)
}


class Fibb {
  constructor(max) {
    this.max = max
  }
  *[Symbol.iterator]() {
    let x = 1
    let y = 1
    let z = x + y
    yield x;
    yield y;
    for (let i = 3; i <= this.max ; i++) {
      yield z;
      x = y
      y = z
      z = x + y
    }
  }
}
let fibb = new Fibb(5)
for(let f of fibb) {
  c(f) // 1, 1, 2, 3, 5
}













