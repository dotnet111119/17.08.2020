console.clear();


function c(msg) { 
  console.log(msg) 
  //console.log(arguments[1])
}

/*
const promise1 = Promise.resolve(3);
const promise2 = fetch('https://jsonplaceholder.typicode.com/posts')
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo');
});

Promise.all([promise1, promise2, promise3]).then((values) => {
  console.log(values[1]);
  values[1].json().then(data => console.log(data))
});
*/
/*
fetch('https://jsonplaceholder.typicode.com/posts')
  .then(response => response.json())
  .then(data => console.log(data));
*/
//setTimeout(function(){ alert("Hello"); }, 3000);
//
function my_ajax(url) {
	const my_promise = new Promise(
        (resolve, reject) => 
  			{
          //alert('promise ...')
          const x = "true"; // prompt("enter value", "true")
          if (x == "true")
            resolve(`Site ${url} returned flights { id , data }`)
          else
            reject(`Site ${url} return error 403`)
        })
  return my_promise
}

my_ajax('mysite.com/getflights').

	// 1
	then(
  
	// this will execute on resolve
	(resolve_data) => {
  		c(`1 resolve with data ${resolve_data}`)
    
      return new Promise(
          (resolve, reject) => 
          {
            //alert('promise ...')
            const x1 = "true"; // prompt("enter value", "true")
            if (x1 == "true")
              resolve(`Site  returned flights { id , data }`)
            else
              reject(`Site  return error 403`)
          })
		},
  
  // this will execute on reject
  (reject_data)=> {
  		console.error(`2 reject with data ${reject_data}`)
		}
  ).

	// 2
	then(
	// this will execute on resolve
	(resolve_data) => {
  		c(`3 resolve with data ${resolve_data}`)
		},
  // this will execute on reject
  (reject_data)=> {
  		console.error(`4 reject with data ${reject_data}`)
		}
  )
/*
fetch('https://jsonplaceholder.typicode.com/posts')
  .then(response => response.json(), err => console.log(`ooops error ${err}`))
  .then(data => console.log(data));
  */



